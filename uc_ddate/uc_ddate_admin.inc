<?php

function uc_ddate_settings_overview() {
  
}

function uc_order_settings_form() {
  $form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Admin settings'),
    '#summary callback' => 'summarize_form',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['admin']['uc_ddate_required'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is delivery date required?'),
    '#summary' => t('Is a Delivery Date Required?'),
    '#default_value' => TRUE,
  );
  return system_settings_form($form);
}
